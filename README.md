# Course Management System

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


### Git Repository
Git: Course Management System https://gitlab.com/jobtestangular/test-angular-gui.git](https://gitlab.com/jobtestangular/test-angular-gui.git)

Clone project from gitlab `git clone https://gitlab.com/jobtestangular/test-angular-gui.git`

> Project Directory `cd test-angular-gui`

### 1. Config Database

> EDIT **dbconnection.js**

<pre>
export const URL_APIS = "http://localhost:3000/" (URL API )
export const IMAGE_PROFILE = "'assets/img/avatars/user.png'" (Default image profile)
</pre>

## 2. Dockerfile 
### 1.  Build Docker image

> **Build command** `docker build -t test-angular-gui:prod .` from **Dockerfile**

### 2. Run Docker container

> **Run command** `docker run --name course_management_system  -d -p 4200:80 --restart=always test-angular-gui:prod`


### 3. Local Run Project

1. install **angular-cli** run `npm install -g @angular/cli`
2. install **package** run `npm install` file **package.json**
3. in directory `test-angular-gui` **RUN** `ng serve`

## 3. Website Course Management System

> **Goto** [http://localhost:4200/](http://localhost:4200/)

