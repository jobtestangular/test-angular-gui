import { Component, OnInit, Input, Inject } from '@angular/core';
import { ViewChild, ElementRef, NgZone, } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Select2OptionData } from 'ng2-select2';
import { Md5 } from 'ts-md5/dist/md5';
import { Compiler } from '@angular/core';
import { ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { URL_APIS } from '../configuration';
import * as moment from 'moment';
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
const md5 = new Md5();

const uri = URL_APIS + 'file/upload';
const uploadurl = URL_APIS + 'images/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

declare var tinyMCE: any;
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, from } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { inherits } from 'util';
import { saveAs } from 'file-saver';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { fbind } from 'q';

@Component({
  selector: 'app-createcourses',
  templateUrl: './createcourses.component.html',
  styleUrls: ['./createcourses.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class CreatecoursesComponent implements OnInit {
  date = new FormControl(moment());
  timeMin = '07:00';
  timeMax = '23:59';
  timeMinEnd = '07:00';
  timeMaxEnd = '23:59';

  constructor(public dialog: MatDialog, private ngZone: NgZone, public rest: RestService, private route: ActivatedRoute, private _compiler: Compiler, private sanitizer: DomSanitizer, private fb: FormBuilder) { }
  public isAddcourse = false;
  public isSubmitCourse = false
  public isInsertCourse = false;
  public isEditCourse = false;
  public isDay = false;
  public isStarttime = false;
  public select2CourseCategory: Array<Select2OptionData>;
  public select2CourseDayOfWeek: Array<Select2OptionData>;
  private coueses_id: any;
  dataSource: MatTableDataSource<CourseData>;
  product: any;
  displayedColumns = ['courses_id', 'name', 'category', 'subject', 'starttime', 'endtime', 'number_student', 'edit'];
  @ViewChild('MatPaginatorUser') MatPaginatorUser: MatPaginator;
  @ViewChild('MatSortUser') MatSortUser: MatSort;

  profileFormCourses = this.fb.group({
    courses_id: ['', Validators.required],
    name: ['', Validators.required],
    description: ['', Validators.required],
    category: ['', Validators.required],
    subject: ['', Validators.required],
    starttime: ['', Validators.required],
    endtime: ['', Validators.required],
    number_student: ['', Validators.required],
    number_student_registered: ['']

  });
  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser.type < 2) {
      window.open('/login', '_self');
    } else {
      this.profileFormCourses.patchValue({
        courses_id: '',
        name: '',
        description: '',
        category: '',
        subject: '',
        starttime: '',
        endtime: '',
        number_student: '',
        number_student_registered: 0
      })

      this.reload();
      this.createCourse()

    }
  }

  checkIDCourse(e: any) {
    if(this.profileFormCourses.value.courses_id != ""){
      let datauser = this.rest.checkCourseID(this.profileFormCourses.value.courses_id).toPromise();
      datauser.then(data => {
        if (data.length != 0) {
          let message = "This couresID is already existed.";
          $(".course_alert").html(message).css({ "color": 'red', "margin-bottom": "5px", "margin-top": "-10px", "float": "right" });
        } else {
          $(".course_alert").html('').css("color", 'green');
  
        }
      });
    }
  
  }

  checkNameCourse(e: any) {
    let dataCheck = {
      name: this.profileFormCourses.value.name
    }
    let datauser = this.rest.checkCourseName(dataCheck).toPromise();
    datauser.then(data => {
      if (data.length != 0) {
        let message = "This coures name \'" + this.profileFormCourses.value.name + "\' is already existed.";
        $(".name_alert").html(message).css({ "color": 'red', "margin-bottom": "5px", "margin-top": "-10px", "float": "right" });
      } else {
        $(".name_alert").html('').css("color", 'green');

      }
    });

  }

  displayAddcourse() {
    this.isDay = true;
    this.isStarttime = true;
    this.profileFormCourses.get('starttime').valueChanges.subscribe(val => {
      this.timeMinEnd = val;

    });

    this.profileFormCourses.get('endtime').valueChanges.subscribe(val => {
      let dataCheck = {
        date: this.profileFormCourses.value.date,
        starttime: this.profileFormCourses.value.starttime,
        endtime: val
      }

    });

    this.isAddcourse = true;
    this.isSubmitCourse = false
    let UserData = JSON.parse(localStorage.getItem('currentUser'));

  }

  async createCourse() {
    this.reload();
    this.isSubmitCourse = true;
    this.isAddcourse = false;
    this.isInsertCourse = true;
    this.isEditCourse = false;
    let data = await this.rest.getCourses().toPromise();
    const course: CourseData[] = [];
    $.each(data, function (index, value) {
      course.push(
        {
          courses_id: value.courses_id,
          name: value.name,
          description: value.description,
          category: value.category,
          subject: value.subject,
          date: moment(value.date).format('LL'),
          starttime: moment(value.starttime, 'HH:mm:ss').format('HH:mm'),
          endtime: moment(value.endtime, 'HH:mm;ss').format('HH:mm'),
          number_student: value.number_student,
          category_name: value.category_name,
          category_name_th: value.category_name_th,
        }
      );

    });

    this.showDataUsers(course);

  }

  showDataUsers(data) {

    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.MatPaginatorUser;
    this.dataSource.sort = this.MatSortUser;

  }

  reload() {
    this.profileFormCourses.patchValue({
      courses_id: '',
      name: '',
      description: '',
      category: '',
      subject: '',
      starttime: '',
      endtime: '',
      number_student: '',
      number_student_registered: 0
    });

    this.isAddcourse = false;
    this.isSubmitCourse = false
    this.createCourseCategory();
    this.createCourseDayOfWek();
  }



  async createCourseCategory() {
    let data = await this.rest.getCourseCategory().toPromise();
    let Category: select2Group[] = [];
    Category.push({ id: "", text: "Select Category" });

    $.each(data, function (index, value) {
      let name = value.name_th + " (" + value.name + ")";

      Category.push({ id: value.category_id, text: name });
    });

    this.select2CourseCategory = Category;

  }

  async createCourseDayOfWek() {
    let data = await this.rest.getCourseDayOfWeek().toPromise();
    let DayOfWeek: select2Group[] = [];
    DayOfWeek.push({ id: "", text: "Select Day" });

    $.each(data, function (index, value) {
      let name = value.name_th + " (" + value.name + ")";

      DayOfWeek.push({ id: value.name, text: name });
    });

    this.select2CourseDayOfWeek = DayOfWeek;

  }

  openDialog(actionID, title, data, dialogclass): void {
    const dialogRef = this.dialog.open(DialogCourse, {
      height: 'auto',
      width: '600px',
      panelClass: 'custom-dialog-container',
      data: {
        actionID: actionID,
        title: title,
        bgclass: dialogclass,
        params: data
      },
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result.actionID == 'insertCourse') {

        this.rest.addCourse(result.params).subscribe((data) => {

          if (data.serverStatus == 2) {
            this.createCourse();
          } else if (data.code == 'ER_DUP_ENTRY') {
            $(".course_alert").html("This couresID is already existed.").css({ "color": 'red', "margin-bottom": "5px", "margin-top": "-10px", "float": "right" });
          } else {

          }

        });
      } else if (result.actionID == 'editCourse') {

        this.rest.editCourse(this.coueses_id, result.params).subscribe((data) => {

          if (data.serverStatus == 2) {
            this.createCourse();
          } else {

          }

        });
      } else if (result.actionID == 'deleteCourse') {

        this.rest.deleteCourse(result.params).subscribe((data) => {

          if (data.serverStatus == 2) {
            this.createCourse();
          } else {

          }

        });
      }
    });
  }

  async checkCourse(id) {
    let datacheckcourse = await this.rest.checkCourse(id).toPromise();
    return datacheckcourse;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  deleteCourse(id) {
    this.openDialog('deleteCourse', 'Do you want to \'DELETE\' Course ?', id, 'trash');
  }

  submitEditCourse() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.openDialog('editCourse', 'Do you want to \'UPDATE\' Course.?', this.profileFormCourses.value, 'warning');

  }

  onSubmitMember() {
    this.openDialog('insertCourse', 'Do you want to \'CREATE\' Course.?', this.profileFormCourses.value, 'plus');

  }

  public updateSelectCategory(e: any): void {
    this.profileFormCourses.patchValue({
      category: e.value,
    });
  }

  public updateSelectDayOfWeek(e: any): void {
    this.profileFormCourses.patchValue({
      date: e.value,
    });
  }

  async editCourse(data) {
    this.profileFormCourses.get('number_student_registered').disable();
    this.profileFormCourses.get('courses_id').disable();

    this.isAddcourse = true;
    this.isSubmitCourse = false
    this.isInsertCourse = false;
    this.isEditCourse = true;
    this.isDay = true;
    this.isStarttime = true;

    this.profileFormCourses.patchValue({
      courses_id: data.courses_id,
      name: data.name,
      description: data.description,
      category: data.category,
      subject: data.subject,
      date: data.date,
      starttime: data.starttime,
      endtime: data.endtime,
      number_student: data.number_student,
    });

    this.coueses_id = data.courses_id;

    this.profileFormCourses.get('starttime').valueChanges.subscribe(val => {
      this.isStarttime = true;
    })
  }

}

export interface DialogData {
  data: string;
  title: string;
  bgclass: string;
}

@Component({
  selector: 'course-dialog',
  templateUrl: 'course-dialog.html',
})

export class DialogCourse {

  constructor(public dialogRef: MatDialogRef<DialogCourse>, @Inject(MAT_DIALOG_DATA) public data: DialogData, public rest: RestService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

export interface select2Group {
  id: string;
  text: string;
}


export interface CourseData {
  courses_id: any;
  name: any;
  description: any;
  category: any;
  subject: any;
  date: any;
  starttime: any;
  endtime: any;
  number_student: any;
  category_name: any;
  category_name_th: any;
}