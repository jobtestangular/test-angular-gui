import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatecoursesComponent } from './createcourses.component';

describe('CreatecoursesComponent', () => {
  let component: CreatecoursesComponent;
  let fixture: ComponentFixture<CreatecoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatecoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatecoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
