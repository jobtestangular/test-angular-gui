import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { URL_APIS } from '../configuration';

const endpoint = URL_APIS;
import { Observable, of } from 'rxjs';
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    login(username: string, password: string) {
        let userSubmit = {
            username: username,
            password: password
        }


        return this.http.post<any>(endpoint + 'members/authenticate', JSON.stringify(userSubmit), httpOptions).pipe(
            map(user => {
                if (user[0] && user[0].token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user[0]));
                }
                return user[0];
            })
        );
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}