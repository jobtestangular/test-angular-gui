import { MatPaginatorIntl } from '@angular/material';

const dutchRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length == 0 || pageSize == 0) { return `0 ของ ${length}`; }

    length = Math.max(length, 0);

    const startIndex = page * pageSize;

    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;

    return `${startIndex + 1} - ${endIndex} ของ ${length}`;
}


export function getDutchPaginatorIntl() {
    const paginatorIntl = new MatPaginatorIntl();

    paginatorIntl.itemsPerPageLabel = 'จำนวนรายการแสดงต่อหน้า:';
    paginatorIntl.nextPageLabel = 'หน้าถัดไป';
    paginatorIntl.previousPageLabel = 'ก่อนหน้า';
    paginatorIntl.firstPageLabel = 'หน้าแรก';
    paginatorIntl.lastPageLabel = 'หน้าสุดท้าย';
    paginatorIntl.getRangeLabel = dutchRangeLabel;

    return paginatorIntl;
}