import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuard } from './_guards/auth.guard';
import { DefaultLayoutComponent } from './containers';
import { CreatemembersComponent } from './createmembers/createmembers.component';
import { CreatecoursesComponent } from './createcourses/createcourses.component';
import { ProfileComponent } from './profile/profile.component'
import { ViewcoursesComponent } from './viewcourses/viewcourses.component'
import { from } from 'rxjs';




const routes: Routes = [
  {
    path: '', canActivate: [AuthGuard],
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  // { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Dashboard'
    },
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      }
    ]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Create Members'
    },
    children: [
      {
        path: 'createmembers',
        component: CreatemembersComponent
      }
    ]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Create Courses'
    },
    children: [
      {
        path: 'createcourses',
        component: CreatecoursesComponent
      }
    ]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'View Courses'
    },
    children: [
      {
        path: 'viewcourse',
        component: ViewcoursesComponent
      }
    ]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Profile'
    },
    children: [
      {
        path: 'profile',
        component: ProfileComponent
      }
    ]
  },
  { path: '**', redirectTo: 'dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
