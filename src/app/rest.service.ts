import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, config, from } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { URL_APIS } from './configuration'

const endpoint = URL_APIS;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  getUserdataLogin(): Observable<any> {
    return this.http.get(endpoint + 'members').pipe(
      map(this.extractData));
  }

  getCourses(): Observable<any> {
    return this.http.get(endpoint + 'courses').pipe(
      map(this.extractData));
  }

  getUserType(): Observable<any> {
    return this.http.get(endpoint + 'members/types').pipe(
      map(this.extractData));
  }

  getCourseCategory(): Observable<any> {
    return this.http.get(endpoint + 'courses/categorys').pipe(
      map(this.extractData));
  }

  getCourseDayOfWeek(): Observable<any> {
    return this.http.get(endpoint + 'courses/getalldays').pipe(
      map(this.extractData));
  }

  getUserGender(): Observable<any> {
    return this.http.get(endpoint + 'members/genders').pipe(
      map(this.extractData));
  }

  getPrenameActive(): Observable<any> {
    return this.http.get(endpoint + 'members/prenameactive').pipe(
      map(this.extractData));
  }
  getPrenameOther(): Observable<any> {
    return this.http.get(endpoint + 'members/prenameother').pipe(
      map(this.extractData));
  }

  getUserLogin(id): Observable<any> {
    return this.http.get(endpoint + 'members/username/' + id).pipe(
      map(this.extractData));
  }

  checkCourseID(id): Observable<any> {
    return this.http.get(endpoint + 'courses/' + id).pipe(
      map(this.extractData));
  }
  getMemberById(id): Observable<any> {
    return this.http.get(endpoint + 'members/' + id).pipe(
      map(this.extractData));
  }

  addUser(master): Observable<any> {
    return this.http.post<any>(endpoint + 'members/add', JSON.stringify(master), httpOptions).pipe(
      tap((master) ),
      catchError(this.handleError<any>('addUser'))
    );
  }

  addCourse(courses): Observable<any> {
    return this.http.post<any>(endpoint + 'courses/add', JSON.stringify(courses), httpOptions).pipe(
      tap(),
      catchError(this.handleError<any>('addCourse'))
    );
  }

  checkCourse(courses): Observable<any> {
    return this.http.post<any>(endpoint + 'courses/check', JSON.stringify(courses), httpOptions).pipe(
      tap(),
      catchError(this.handleError<any>('checkCourse'))
    );
  }

  checkCourseName(courses): Observable<any> {
    return this.http.post<any>(endpoint + 'courses/checkname', JSON.stringify(courses), httpOptions).pipe(
      tap((courses)),
      catchError(this.handleError<any>('checkCourseName'))
    );
  }

  
  searchCourse(courses): Observable<any> {
    return this.http.post<any>(endpoint + 'courses/search', JSON.stringify(courses), httpOptions).pipe(
      tap((courses)),
      catchError(this.handleError<any>('searchCourse'))
    );
  }
  editMember(username, master): Observable<any> {
    return this.http.put(endpoint + 'members/update/' + username, JSON.stringify(master), httpOptions).pipe(
      tap(),
      catchError(this.handleError<any>('updateUserdata'))
    );
  }

  editCourse(courseid, coursedata): Observable<any> {
    return this.http.put(endpoint + 'courses/update/' + courseid, JSON.stringify(coursedata), httpOptions).pipe(
      tap(),
      catchError(this.handleError<any>('editCourse'))
    );
  }

  deleteUser(id): Observable<any> {
    return this.http.delete<any>(endpoint + 'members/delete/' + id, httpOptions).pipe(
      tap(),
      catchError(this.handleError<any>('deleteProduct'))
    );
  }

  deleteCourse(id): Observable<any> {
    return this.http.delete<any>(endpoint + 'courses/delete/' + id, httpOptions).pipe(
      tap(),
      catchError(this.handleError<any>('deleteCourse'))
    );
  }

  authenticate(user): Observable<any> {
    return this.http.post<any>(endpoint + 'members/authenticate', JSON.stringify(user), httpOptions).pipe(
      tap((user)),
      catchError(this.handleError<any>('authenticate'))
    );
  }

  deleteHomeimage(imagename): Observable<any> {
    return this.http.post<any>(endpoint + 'file/delete', JSON.stringify(imagename), httpOptions).pipe(
      tap((imagename)),
      catchError(this.handleError<any>('deleteServicesByMore'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}