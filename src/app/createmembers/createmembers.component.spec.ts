import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatemembersComponent } from './createmembers.component';

describe('CreatemembersComponent', () => {
  let component: CreatemembersComponent;
  let fixture: ComponentFixture<CreatemembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatemembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatemembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
