import { Component, OnInit, Input, Inject } from '@angular/core';
import { ViewChild, ElementRef, NgZone, } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Select2OptionData } from 'ng2-select2';
import { Md5 } from 'ts-md5/dist/md5';
import { Compiler } from '@angular/core';
import { ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { URL_APIS } from '../configuration';
import * as moment from 'moment';
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
const md5 = new Md5();

const uri = URL_APIS + 'file/upload';
const uploadurl = URL_APIS + 'images/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

declare var tinyMCE: any;
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, from } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { inherits } from 'util';
import { saveAs } from 'file-saver';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { fbind } from 'q';

@Component({
  selector: 'app-createmembers',
  templateUrl: './createmembers.component.html',
  styleUrls: ['./createmembers.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})

export class CreatemembersComponent implements OnInit {
  date = new FormControl(moment());
  maxDate = new Date();

  constructor(public dialog: MatDialog, private ngZone: NgZone, public rest: RestService, private route: ActivatedRoute, private _compiler: Compiler, private sanitizer: DomSanitizer, private fb: FormBuilder) { }
  public filePreviewPath: SafeUrl;
  public currentUser = "";
  uploader: FileUploader = new FileUploader({ url: uri });
  public uploaderHomeFile: any = [];
  attachmentList: any = [];
  private user_id: any;
  public isAddmember = false;
  public isSubmitMember = false
  public isInsertMember = false;
  public isEditMember = false;
  public select2PrefixnameActive: Array<Select2OptionData>;
  public select2UserGroup: Array<Select2OptionData>;
  public select2UserGender: Array<Select2OptionData>;
  dataSource: MatTableDataSource<UserData>;
  product: any;
  displayedColumns = ['username', 'name', 'nickname', 'birthday', 'gender', 'type','updatedate', 'editby', 'edit'];
  @ViewChild('MatPaginatorUser') MatPaginatorUser: MatPaginator;
  @ViewChild('MatSortUser') MatSortUser: MatSort;

  profileFormMember = this.fb.group({
    id: [''],
    username: ['', Validators.required],
    password: [''],
    repassword: [''],
    prename: ['', Validators.required],
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    nickname: [''],
    gender: [''],
    birthday: [''],
    mobile: [''],
    email: ['', Validators.required],
    type: ['', Validators.required],
    image: ['', Validators.required],
    view: ['', Validators.required],
    edit: ['', Validators.required],
    del: ['', Validators.required],
    token: [''],
    facebook: [''],
    line: [''],
    insert_user_id: [''],
    insert_datetime: [''],
    update_user_id: [''],
  });

  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.type < 2){
        window.open('/login','_self');
    }else{
      this.profileFormMember.patchValue({
        view: true,
        edit: true,
        del: true,
      })
      this.attachmentList = [];
      this.uploader.queue = [];
      this.filePreviewPath = "assets/img/avatars/user.png";
      this.loadUploader();
      this.reload();
      this.createUserGroup()
  
    }
  }

  displayAddmember() {
    this.profileFormMember.get('username').valueChanges.subscribe(val => {
      let datauser = this.getUser(val);
      if (this.isEditMember == true) {
        $(".username_alert").html("");
        return;

      }

      if (val != "") {
        datauser.then(data => {
          if (data.length != 0) {
            let message = "Username " + val + " Already in the system!";
            $(".username_alert").html(message).css("color", 'red');
          } else {
            let message = "Username " + val + " is available.";
            $(".username_alert").html(message).css("color", 'green');

          }
        });
      } else {
        $(".username_alert").html("").css("color", 'green');
      }

    });


    this.profileFormMember.get('email').valueChanges.subscribe(val => {
      if (val != "") {
        if (!this.validateEmail(val)) {
          let message = "รูปแบบ Email: " + val + " ไม่ถูกต้อง.";
          $(".email_alert").html(message).css("color", 'red');
        } else {
          let message = "สามารถใช้ Email: " + val + " ได้.";

          $(".email_alert").html(message).css("color", 'green');

        }
      } else {
        $(".email_alert").html("").css("color", 'green');
      }

    });

    this.profileFormMember.get('repassword').enable();
    this.profileFormMember.get('repassword').valueChanges.subscribe(val => {
      if (val != this.profileFormMember.value.password) {
        $(".repassword_alert").html("Password not match!.").css("color", 'red');
      } else {
        $(".repassword_alert").html("Password is match, You can use this password. ").css("color", 'green');

      }
    });


    this.isAddmember = true;
    this.isSubmitMember = false
    this.createPrenameActive();
    let UserData = JSON.parse(localStorage.getItem('currentUser'));
    this.profileFormMember.patchValue({
      insert_user_id: UserData.username,
      insert_datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
      view: true,
      edit: false,
      del: false
    })

  }

  async createUserGroup() {
    this.reload();
    this.isSubmitMember = true;
    this.isAddmember = false;
    this.isInsertMember = true;
    this.isEditMember = false;
    await this.createPrenameActive();
    let data = await this.rest.getUserdataLogin().toPromise();
    const users: UserData[] = [];
    $.each(data, function (index, value) {
      var name = value.user_prename + value.user_firstname + " " + value.user_lastname;
      users.push(
        {
          id: value.id,
          username: value.username,
          prename: value.prename,
          firstname: value.firstname,
          lastname: value.lastname,
          nickname: value.nickname,
          gender: value.gender,
          birthday: moment(value.birthday).format('LL'),
          mobile: value.mobile,
          email: value.email,
          type: value.type,
          image: value.image,
          view: value.view,
          edit: value.edit,
          del: value.del,
          token: value.token,
          facebook: value.facebook,
          line: value.line,
          insert_user_id: value.insert_user_id,
          insert_datetime: moment(value.insert_datetime).format('LL'),
          update_user_id: value.update_user_id,
          update_datetime: moment(value.update_datetime).format('LL'),
          gender_name:value.gender_name,
          prename_th:value.prename_th,
          type_name:value.type_name
        }
      );

    });

    this.showDataUsers(users);

  }

  showDataUsers(data) {

    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.MatPaginatorUser;
    this.dataSource.sort = this.MatSortUser;

  }

  reload() {
    this.profileFormMember.patchValue({
      id: '',
      username: '',
      password: '',
      repassword: [''],
      prename: '',
      firstname: '',
      lastname: '',
      nickname: '',
      gender: '',
      birthday: '',
      mobile: '',
      email: '',
      type: '',
      image: '',
      view: false,
      edit: false,
      del: false,
      token: '',
      facebook: '',
      line: '',
      insert_user_id: '',
      insert_datetime: '',
      update_user_id: '',
    });

    this.isAddmember = false;
    this.isSubmitMember = false
    this.createPrenameActive();
    this.createUserType();
    this.createUserGender();
  }

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.profileFormMember.patchValue({
      birthday: moment(event.value).format('YYYY-MM-DD')
    })
  }

  loadUploader() {
    this.uploader = new FileUploader({ url: uri });
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
      this.filePreviewPath = this.sanitizer.bypassSecurityTrustUrl((window.URL.createObjectURL(file._file)));
      if (this.uploader.queue.length > 1) {
        this.uploader.removeFromQueue(this.uploader.queue[0]);
      }
    };
    this.uploader.onBuildItemForm = (item, form) => {
      form.append("filepath", "members/profiles");
      form.append("fileurl", uploadurl);

    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      let dataImage = JSON.parse(response);
      // this.masterDatas.profile_image = dataImage.url;
      this.profileFormMember.patchValue({
        image: dataImage.url,

      });
    }
  }

  async createPrenameActive() {
    let data = await this.rest.getPrenameActive().toPromise();
    let prefix: select2Group[] = [];
    prefix.push({ id: "", text: "Select prefix name" });

    $.each(data, function (index, value) {
      prefix.push({ id: value.pren_id, text: value.prename_th });
    });

    this.select2PrefixnameActive = prefix;

  }

  async createUserType() {
    let data = await this.rest.getUserType().toPromise();
    let type: select2Group[] = [];
    type.push({ id: "", text: "Select type" });

    $.each(data, function (index, value) {
      let name = value.name_th + " (" + value.name + ")";

      type.push({ id: value.type_id, text: name });
    });

    this.select2UserGroup = type;

  }

  async createUserGender() {
    let data = await this.rest.getUserGender().toPromise();
    let gender: select2Group[] = [];
    gender.push({ id: "", text: "Select gender" });

    $.each(data, function (index, value) {
      let name = value.name_th + " (" + value.name + ")";

      gender.push({ id: value.type_id, text: name });
    });

    this.select2UserGender = gender;

  }

  openDialog(actionID, title, data, dialogclass): void {
    const dialogRef = this.dialog.open(DialogMember, {
      height: 'auto',
      width: '600px',
      panelClass: 'custom-dialog-container',
      data: {
        actionID: actionID,
        title: title,
        bgclass: dialogclass,
        params: data
      },
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result.actionID == 'insertUser') {

        this.rest.addUser(result.params).subscribe((data) => {

          if (data.serverStatus == 2) {
            this.createUserGroup();
          } else {

          }

        });
      } else if (result.actionID == 'editMember') {

        this.rest.editMember(this.user_id, result.params).subscribe((data) => {

          if (data.serverStatus == 2) {
            this.createUserGroup();
          } else {

          }

        });
      }else if (result.actionID == 'deleteUser') {

        this.rest.deleteUser(result.params).subscribe((data) => {

          if (data.serverStatus == 2) {
            this.createUserGroup();
          } else {

          }

        });
      }
    });
  }

  async getUser(id) {
    let datacheckuser = await this.rest.getUserLogin(id).toPromise();
    return datacheckuser;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  deleteMaster(id) {
    this.openDialog('deleteUser', 'Do you want to \'DELETE\' Member ?', id, 'trash');
  }

  submitEditMember() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.profileFormMember.patchValue({
      update_user_id: currentUser.username
    })
    this.profileFormMember.get('repassword').disable();
    this.profileFormMember.get('password').disable();
    this.profileFormMember.get('insert_datetime').disable();
    this.profileFormMember.get('id').disable();

    this.openDialog('editMember', 'Do you want to \'UPDATE\' Member ?', this.profileFormMember.value, 'warning');

  }

  onSubmitMember() {
    // TODO: Use EventEmitter with form value
    if (this.profileFormMember.value.password != '' && this.profileFormMember.value.repassword != '') {
      if (this.profileFormMember.value.password != this.profileFormMember.value.repassword) {
        this.profileFormMember.patchValue({
          repassword: '',
        })

        $(".repassword_alert").val('Password is not match!');

      } else {
        let password_md5 = new Md5();
        let password_en = password_md5.appendStr(this.profileFormMember.value.repassword).end();
        let token = new Md5();
        let token_en = token.appendStr(this.profileFormMember.value.username).end();
        this.profileFormMember.patchValue({
          password: password_en,
          repassword: '',
          token: token_en,
        })
        let UserData = JSON.parse(localStorage.getItem('currentUser'));
        this.profileFormMember.patchValue({
          insert_user_id: UserData.username,
          insert_datetime: moment().format('YYYY-MM-DD HH:mm:ss'),
          view: true,
          edit: false,
          del: false
        })

        this.profileFormMember.get('repassword').disable();
        this.profileFormMember.get('id').disable();

        $(".repassword_alert").val('');
        this.openDialog('insertUser', 'Do you want to \'CREATE\' Member ?', this.profileFormMember.value, 'plus');
      }
    }
  }

  public updateSelectPrename(e: any): void {
    this.profileFormMember.patchValue({
      prename: e.value,
    });
    if (e.value == '1' || e.value == '3') {
      this.profileFormMember.patchValue({
        gender: '1',
      });
    } else if (e.value == '2' || e.value == '4' || e.value == '5') {
      this.profileFormMember.patchValue({
        gender: '2',
      });
    }
  }

  public updateSelectGroup(e: any): void {
    this.profileFormMember.patchValue({
      type: e.value,
    });

  }

  public updateSelectGender(e: any): void {
    this.profileFormMember.patchValue({
      gender: e.value,
    });
  }

  async validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  async editMaster(data) {
    this.profileFormMember.get('id').disable();
    this.isAddmember = true;
    this.isSubmitMember = false
    this.isInsertMember = false;
    this.isEditMember = true;
    this.filePreviewPath = data.image;
    this.profileFormMember.patchValue({
      id: data.id,
      username: data.username,
      prename: data.prename,
      firstname: data.firstname,
      lastname: data.lastname,
      nickname: data.nickname,
      gender: data.gender,
      birthday: moment(data.birthday).format('YYYY-MM-DD'),
      mobile: data.mobile,
      email: data.email,
      type: data.type,
      image: data.image,
      view: data.view,
      edit: data.edit,
      del: data.del,
      token: data.token,
      facebook: data.facebook,
      line: data.line,
    });

    this.user_id = data.id;
    $(".repassword_alert").html("");
    $(".username_alert").html("");
    $(".email_alert").html("");
  }

}

export interface DialogData {
  data: string;
  title: string;
  bgclass: string;
}

@Component({
  selector: 'member-dialog',
  templateUrl: 'member-dialog.html',
})

export class DialogMember {

  constructor(public dialogRef: MatDialogRef<DialogMember>, @Inject(MAT_DIALOG_DATA) public data: DialogData, public rest: RestService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

export interface select2Group {
  id: string;
  text: string;
}

export interface UserData {
  id: any;
  username: any;
  prename: any;
  firstname: any;
  lastname: any;
  nickname: any;
  gender: any;
  birthday: any;
  mobile: any;
  email: any;
  type: any;
  image: any;
  view: any;
  edit: any;
  del: any;
  token: any;
  facebook: any;
  line: any;
  insert_user_id: any;
  insert_datetime: any;
  update_user_id: any;
  update_datetime: any;
  gender_name:any;
  type_name:any;
  prename_th : any;
}