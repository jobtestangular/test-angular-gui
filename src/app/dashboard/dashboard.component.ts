import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public showEditProfile = false;
  public showCreateCourse = false;
  public showViewCourse = false;
  public showCreateMember = false;


  constructor() { }

  goto(data) {
    window.open('#/' + data, '_self');
  }
  ngOnInit() {
    let userdata = localStorage.getItem('currentUser');

    if (userdata != '') {
      let permitUser = JSON.parse(userdata);
      if (permitUser.type == '2') {
        this.showEditProfile = true;
        this.showCreateCourse = true;
        this.showViewCourse = true;
        this.showCreateMember = false;

      } else if (permitUser.type == '1') {
        this.showEditProfile = true;
        this.showCreateCourse = false;
        this.showViewCourse = true;
        this.showCreateMember = false;
      } else if (permitUser.type == '3') {
        this.showEditProfile = true;
        this.showCreateCourse = true;
        this.showViewCourse = true;
        this.showCreateMember = true;
      }
    }
  }

}
