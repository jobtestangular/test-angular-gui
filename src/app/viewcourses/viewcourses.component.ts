import { Component, OnInit, Input, Inject } from '@angular/core';
import { ViewChild, ElementRef, NgZone, } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { FileSelectDirective, FileUploader } from 'ng2-file-upload';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Select2OptionData } from 'ng2-select2';
import { Md5 } from 'ts-md5/dist/md5';
import { Compiler } from '@angular/core';
import { ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { URL_APIS } from '../configuration';
import * as moment from 'moment';
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};
const md5 = new Md5();

const uri = URL_APIS + 'file/upload';
const uploadurl = URL_APIS + 'images/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

const colorran = [
  '#caf3d0',
  '#d4e6ff',
  '#f26b31',
  '#d1dbe4'
]
declare var tinyMCE: any;
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, from } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { inherits } from 'util';
import { saveAs } from 'file-saver';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { fbind } from 'q';
import {
  ChangeDetectionStrategy,
  TemplateRef
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarDateFormatter,
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
  DAYS_OF_WEEK
} from 'angular-calendar';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};
import { CustomDateFormatter } from './custom-date-formatter.provider';

@Component({
  selector: 'app-viewcourses',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './viewcourses.component.html',
  styleUrls: ['./viewcourses.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ],
})
export class ViewcoursesComponent implements OnInit {
  date = new FormControl(moment());
  timeMin = '07:00:00';
  timeMinEnd = '07:00:00';
  constructor(private modal: NgbModal, public dialog: MatDialog, private ngZone: NgZone, public rest: RestService, private route: ActivatedRoute, private _compiler: Compiler, private sanitizer: DomSanitizer, private fb: FormBuilder) { }
  public isAddcourse = false;
  public isSubmitCourse = false
  public isInsertCourse = false;
  public isEditCourse = false;
  public isDay = false;
  public isStarttime = false;
  public select2CourseCategory: Array<Select2OptionData>;
  public select2CourseDayOfWeek: Array<Select2OptionData>;
  private coueses_id: any;
  dataSource: MatTableDataSource<CourseData>;
  product: any;
  displayedColumns = ['courses_id', 'name', 'category', 'subject', 'starttime', 'endtime', 'number_student'];
  @ViewChild('MatPaginatorUser') MatPaginatorUser: MatPaginator;
  @ViewChild('MatSortUser') MatSortUser: MatSort;

  profileFormCourses = this.fb.group({
    courses_id: ['', Validators.required],
    name: ['', Validators.required],
    description: ['', Validators.required],
    category: ['', Validators.required],
    subject: ['', Validators.required],
    starttime: ['', Validators.required],
    endtime: ['', Validators.required],
    number_student: ['', Validators.required],
    number_student_registered: ['']

  });

  profileSearchCourses = this.fb.group({
    name: [''],
    starttime: [''],
    endtime: [''],
  });
  // CALEN
  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Day;
  viewDate: Date = new Date();
  locale: string = 'en';
  weekStartsOn: number = DAYS_OF_WEEK.MONDAY;
  weekendDays: number[] = [DAYS_OF_WEEK.FRIDAY, DAYS_OF_WEEK.SATURDAY];
  CalendarView = CalendarView;

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();
  start = moment().format();
  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = true;

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map(iEvent => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    for (let index = 0; index < 10; index++) {
      this.events.push({
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      });
    }

  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter(event => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }
  // END
  ngOnInit() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.profileSearchCourses.get('starttime').valueChanges.subscribe(val => {
      this.timeMinEnd = val;
    });
    this.profileFormCourses.patchValue({
      courses_id: '',
      name: '',
      description: '',
      category: '',
      subject: '',
      starttime: '',
      endtime: '',
      number_student: '',
      number_student_registered: 0
    })

    this.profileSearchCourses.patchValue({
      name: '',
      starttime: '',
      endtime: '',
    })
    this.reload();
    this.createCourse()
  }

  async createCourse() {
    this.reload();
    this.isSubmitCourse = true;
    this.isAddcourse = false;
    this.isInsertCourse = true;
    this.isEditCourse = false;
    let data = await this.rest.getCourses().toPromise();
    const course: CourseData[] = [];
    $.each(data, function (index, value) {
      course.push(
        {
          courses_id: value.courses_id,
          name: value.name,
          description: value.description,
          category: value.category,
          subject: value.subject,
          starttime: value.starttime,
          endtime: value.endtime,
          number_student: value.number_student,
          category_name: value.category_name,
          category_name_th: value.category_name_th,
        }
      );

    });

    this.events = [];
    for (let i = 0; i < data.length; i++) {
      let name = "<b>" + data[i].courses_id + "</b><br>" + data[i].name;
      let startdate = moment().format('YYYY-MM-DD') + " " + data[i].starttime;
      let enddate = moment().format('YYYY-MM-DD') + " " + data[i].endtime;
      this.events.push({
        title: name,
        start: new Date(startdate),
        end: new Date(enddate),
        color: {
          primary: "#e3bc08",
          secondary: "#FDF1BA"
        },
        meta: data[i]

      });
    }


    this.showDataUsers(course);

  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max)) - 1;
  }

  showDataUsers(data) {

    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.MatPaginatorUser;
    this.dataSource.sort = this.MatSortUser;

  }

  reload() {
    this.profileFormCourses.patchValue({
      courses_id: '',
      name: '',
      description: '',
      category: '',
      subject: '',
      starttime: '',
      endtime: '',
      number_student: '',
      number_student_registered: 0
    });

    this.isAddcourse = false;
    this.isSubmitCourse = false
    this.createCourseCategory();
    this.createCourseDayOfWek();
  }

  async createCourseCategory() {
    let data = await this.rest.getCourseCategory().toPromise();
    let Category: select2Group[] = [];
    Category.push({ id: "", text: "Select Category" });

    $.each(data, function (index, value) {
      let name = value.name_th + " (" + value.name + ")";

      Category.push({ id: value.category_id, text: name });
    });

    this.select2CourseCategory = Category;

  }

  async createCourseDayOfWek() {
    let data = await this.rest.getCourseDayOfWeek().toPromise();
    let DayOfWeek: select2Group[] = [];
    DayOfWeek.push({ id: "", text: "Select Day" });
    $.each(data, function (index, value) {
      let name = value.name_th + " (" + value.name + ")";

      DayOfWeek.push({ id: value.name, text: name });
    });

    this.select2CourseDayOfWeek = DayOfWeek;

  }

  openDialog(actionID, title, data, dialogclass): void {
    const dialogRef = this.dialog.open(DialogViewCourse, {
      height: 'auto',
      width: '600px',
      panelClass: 'custom-dialog-container',
      data: {
        actionID: actionID,
        title: title,
        bgclass: dialogclass,
        params: data
      },
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result.actionID == 'insertCourse') {

        this.rest.addCourse(result.params).subscribe((data) => {

          if (data.serverStatus == 2) {
            this.createCourse();
          } else {

          }

        });
      } else if (result.actionID == 'editCourse') {

        this.rest.editCourse(this.coueses_id, result.params).subscribe((data) => {

          if (data.serverStatus == 2) {
            this.createCourse();
          } else {

          }

        });
      } else if (result.actionID == 'deleteCourse') {

        this.rest.deleteCourse(result.params).subscribe((data) => {

          if (data.serverStatus == 2) {
            this.createCourse();
          } else {

          }

        });
      }
    });
  }

  async checkCourse(id) {
    let datacheckcourse = await this.rest.checkCourse(id).toPromise();
    return datacheckcourse;

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  deleteCourse(id) {
    this.openDialog('deleteCourse', 'Do you want to \'DELETE\' Course ?', id, 'trash');
  }

  submitEditCourse() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.openDialog('editCourse', 'Do you want to \'UPDATE\' Course.?', this.profileFormCourses.value, 'warning');

  }

  onSubmitMember() {

    this.openDialog('insertCourse', 'Do you want to \'CREATE\' Course.?', this.profileFormCourses.value, 'plus');

  }

  public updateSelectCategory(e: any): void {
    this.profileFormCourses.patchValue({
      category: e.value,
    });
  }

  public updateSelectCategorySearch(e: any): void {
    this.profileSearchCourses.patchValue({
      category: e.value,
    });
  }
  public updateSelectDayOfWeek(e: any): void {
    this.profileFormCourses.patchValue({
      date: e.value,
    });
  }

  async searchCourse() {

    let data = await this.rest.searchCourse(this.profileSearchCourses.value).toPromise();
    const course: CourseData[] = [];
    $.each(data, function (index, value) {
      course.push(
        {
          courses_id: value.courses_id,
          name: value.name,
          description: value.description,
          category: value.category,
          subject: value.subject,
          starttime: value.starttime,
          endtime: value.endtime,
          number_student: value.number_student,
          category_name: value.category_name,
          category_name_th: value.category_name_th,
        }
      );

    });

    this.events = [];
    for (let i = 0; i < data.length; i++) {
      let name = "<b>" + data[i].courses_id + "</b><br>" + data[i].name;
      let startdate = moment().format('YYYY-MM-DD') + " " + data[i].starttime;
      let enddate = moment().format('YYYY-MM-DD') + " " + data[i].endtime;
      this.events.push({
        title: name,
        start: new Date(startdate),
        end: new Date(enddate),
        color: {
          primary: "#e3bc08",
          secondary: "#FDF1BA"
        },
        meta: data[i]

      });
    }


    this.showDataUsers(course);

  }

  async editCourse(data) {

    this.profileFormCourses.get('number_student_registered').disable();
    this.profileFormCourses.get('courses_id').disable();

    this.isAddcourse = true;
    this.isSubmitCourse = false
    this.isInsertCourse = false;
    this.isEditCourse = true;
    this.isDay = true;
    this.isStarttime = true;

    this.profileFormCourses.patchValue({
      courses_id: data.courses_id,
      name: data.name,
      description: data.description,
      category: data.category,
      subject: data.subject,
      date: data.date,
      starttime: data.starttime,
      endtime: data.endtime,
      number_student: data.number_student,
    });

    this.coueses_id = data.courses_id;

    this.profileFormCourses.get('starttime').valueChanges.subscribe(val => {
      this.isStarttime = true;
    })
    this.profileFormCourses.get('endtime').valueChanges.subscribe(val => {
      let dataCheck = {
        date: this.profileFormCourses.value.date,
        starttime: this.profileFormCourses.value.starttime,
        endtime: val
      }
      let datauser = this.checkCourse(dataCheck);
      datauser.then(data => {

      });
    });

  }

}

export interface DialogData {
  data: string;
  title: string;
  bgclass: string;
}

@Component({
  selector: 'viewcourses-dialog',
  templateUrl: 'viewcourses-dialog.html',
})

export class DialogViewCourse {

  constructor(public dialogRef: MatDialogRef<DialogViewCourse>, @Inject(MAT_DIALOG_DATA) public data: DialogData, public rest: RestService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

export interface select2Group {
  id: string;
  text: string;
}


export interface CourseData {
  courses_id: any;
  name: any;
  description: any;
  category: any;
  subject: any;
  starttime: any;
  endtime: any;
  number_student: any;
  category_name: any;
  category_name_th: any;
}