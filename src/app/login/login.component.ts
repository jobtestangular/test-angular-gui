import { Component, OnInit, Injectable } from '@angular/core';
import * as $ from 'jquery';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { RestService } from '../rest.service';
import { AuthenticationService } from '../_guards/authentication.service';
import { Md5 } from 'ts-md5/dist/md5';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  public alert = '';
  private password_en;

  dataFormLogin = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  })

  constructor(
    private fb: FormBuilder,
    public rest: RestService,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
    this.authenticationService.logout();

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
  }

  get f() { return this.dataFormLogin.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.dataFormLogin.invalid) {
      return;
    }

    this.loading = true;
    let password_md5 = new Md5();
    this.password_en = password_md5.appendStr(this.f.password.value).end();

    this.authenticationService.login(this.f.username.value, this.password_en)
      .pipe(first())
      .subscribe(
        data => {
          if (!data) {
            $('.login_alert').html('Username or Password Incorrect.');

          }
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          this.loading = false;
        });

  }

  async autentication(){
  }

  async register(){
  }

}
