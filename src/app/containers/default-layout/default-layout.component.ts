import { Component, Input, OnInit } from '@angular/core';
import { IMAGE_PROFILE } from '../../configuration';
import { RestService } from '../../rest.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',

})
export class DefaultLayoutComponent implements OnInit {
  public navItems = [];
  public currentUser;
  permitUser = new Object;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  constructor(public rest: RestService) {

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true
    });
  }

  async getCurrentUser(memberid) {
    let data = await this.rest.getMemberById(memberid).toPromise();
    this.currentUser = data[0];

  }

  ngOnInit() {
    this.navItems = [];
    let userdata = localStorage.getItem('currentUser');

    if (userdata != '') {
      let permitUser = JSON.parse(userdata);
      this.currentUser = permitUser;
      if (this.currentUser.image == '') {
        this.currentUser.image = IMAGE_PROFILE;
      }
      let profilename = [];
      if (this.currentUser.firstname != '') {
        profilename.push(this.currentUser.firstname);
      }

      if (this.currentUser.lastname) {
        profilename.push(this.currentUser.lastname);
      }

      if (profilename) {
        this.currentUser.profilename = profilename.join(" ");
      }

      if (permitUser.type == '2') {
        this.navItems.push(
          {
            name: ' Course Management System',
            url: '/dashboard',
            icon: 'fa fa-home',
            badge: {
              variant: 'info',
              text: ''
            }
          },
          {
            name: ' Edit Profile',
            url: '/profile',
            icon: 'fa fa-address-book-o',
          },
          {
            name: ' Create Course',
            url: '/createcourses',
            icon: 'fa fa-book',
          },
          {
            name: ' View Course',
            url: '/viewcourse',
            icon: 'fa fa-search',
          },
          {
            name: ' Logout',
            url: '/login',
            icon: 'fa fa-sign-out',
          }
        );
      } else if (permitUser.type == '1') {
        this.navItems.push(
          {
            name: ' Course Management System',
            url: '/dashboard',
            icon: 'fa fa-home',
            badge: {
              variant: 'info',
              text: ''
            }
          },
          {
            name: ' Edit Profile',
            url: '/profile',
            icon: 'fa fa-address-book-o',
          },
          {
            name: ' View Course',
            url: '/viewcourse',
            icon: 'fa fa-search',
          },
          {
            name: ' Logout',
            url: '/login',
            icon: 'fa fa-sign-out',
          }
        );
      } else if (permitUser.type == '3') {
        this.navItems.push(
          {
            name: ' Course Management System',
            url: '/dashboard',
            icon: 'fa fa-home',
            badge: {
              variant: 'info',
              text: ''
            }
          },
          {
            name: ' Create Members',
            url: '/createmembers',
            icon: 'fa fa-user-plus',
          },
          {
            name: ' Edit Profile',
            url: '/profile',
            icon: 'fa fa-address-book-o',
          },
          {
            name: ' Create Course',
            url: '/createcourses',
            icon: 'fa fa-book',
          },
          {
            name: ' View Course',
            url: '/viewcourse',
            icon: 'fa fa-search',
          },
          {
            name: ' Logout',
            url: '/login',
            icon: 'fa fa-sign-out',
          }
        );
      }

      this.getCurrentUser(permitUser.id)


    }
  }
}
